# Generated by Django 2.2.10 on 2020-04-02 07:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0006_auto_20200402_0708'),
    ]

    operations = [
        migrations.AlterField(
            model_name='costdata',
            name='commisstion_member',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='costdata',
            name='contract_attachmentes',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='costdata',
            name='contract_termination_information',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='costdata',
            name='contractor_selection',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='costdata',
            name='leave_reason',
            field=models.TextField(default=1),
            preserve_default=False,
        ),
    ]
